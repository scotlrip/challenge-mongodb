let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let app = express();

let { CATEGORY_ROUTE } = require('./routes/Category');
let { PRODUCT_ROUTE }  = require('./routes/Product');
let { TAG_ROUTE } = require('./routes/Tag');
let { PRODUCT_TAG_ROUTE } = require('./routes/ProductTag');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/category', CATEGORY_ROUTE);
app.use('/product', PRODUCT_ROUTE);
app.use('/tag', TAG_ROUTE);
app.use('/product-tag',PRODUCT_TAG_ROUTE);

app.get('/', (req, res) => {
    res.json({ message: 'server connected' });
});

let uri = `mongodb://localhost/mongo_ref`;

mongoose.connect(uri);
mongoose.connection.once('open', () => {
    console.log(`mongodb connected`);
    app.listen(3000, () => console.log(`server start at port 3000`));
});
