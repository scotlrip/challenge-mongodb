let mongoose = require('mongoose');
let Schema = mongoose.Schema;


let tagSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    description: String,
    products: [
        {
            type: Schema.Types.ObjectId,
            ref:'product'
        }
    ]
});

let TagModel = mongoose.model('tag', tagSchema);
exports.TAG_MODEL = TagModel;