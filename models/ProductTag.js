let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let productTagSchema = new Schema({
    productID:{
        type:Schema.Types.ObjectId,
        ref:'product'
    },
    tagID: {
        type:Schema.Types.ObjectId,
        ref:'tag'
    }
});

let ProductTagModel = mongoose.model('product_tag', productTagSchema);
exports.PRODUCT_TAG_MODEL = ProductTagModel;
