let express = require('express');
let route = express.Router();
let ObjectID = require('mongoose').Types.ObjectId;
let {PRODUCT_TAG_MODEL} = require('../models/ProductTag');
let { PRODUCT_MODEL } = require('../models/Product');
let { TAG_MODEL } = require('../models/Tag');


route.post('/add-product-tag', async (req, res) =>{
    try {
        let {productID, tagID } = req.body;
        if(!ObjectID.isValid(productID) || !ObjectID.isValid(tagID)) res.json({error:true, message:"Object ID invalid"});
        let infoProductTag = new PRODUCT_TAG_MODEL({
            productID, tagID
        });
        let infoProductTagAfterInsert = await infoProductTag.save();
        if(!infoProductTagAfterInsert) res.json({error:true, message:"can not insert product tag"});
        
        let infoProductAfterUpdate = await PRODUCT_MODEL.findByIdAndUpdate(productID,{
            $push:{
                tags:tagID
            }
        })
        let infoTagAfterUpdate = await TAG_MODEL.findByIdAndUpdate(tagID,{
            $push:{
                products: productID
            }
        })
        if(!infoProductAfterUpdate || !infoTagAfterUpdate) res.json({error:true, message:'can not update product or tag'});
        
        res.json({error:false, data: infoTagAfterUpdate,infoProductAfterUpdate}); 
    } catch (error) {
        res.json({error:true, message:error.message});
    }
    
});
route.get('/list-product-tag',async (req, res) => {
    try {
        let listProductTag = await PRODUCT_TAG_MODEL.find({});
        res.json({error:false, data:listProductTag});
    } catch (error) {
        res.json({error:true, message:error.message});
    }
    
})
exports.PRODUCT_TAG_ROUTE = route;