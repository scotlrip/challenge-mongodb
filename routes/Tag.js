let express = require('express');
let route = express.Router();
let { PRODUCT_MODEL } = require('../models/Product');
let { TAG_MODEL } = require('../models/Tag');


route.post('/add-tag', async(req, res) => {
    try {
        let { title, description } = req.body;
        let infoTag = new TAG_MODEL({
            title,description
        });
        let infoTagAfterInsert = await infoTag.save();
        if(!infoTagAfterInsert) res.json({error:true, message:'can not insert tag'});
        res.json({error:false, data:infoTagAfterInsert});
    } 
    catch (error) {
        res.json({error:true, message:error.message})
    }
   

})
route.get('/list-tag-all-product', async (req, res) => {
    try {
        let listTag = await PRODUCT_MODEL.find({}).populate({
            path:'tags',
            select:'title description'
        })
        res.json({error:false, data: listTag});
    } catch (error) {
        res.json({error:true, message:error.message})
    }
    
});

exports.TAG_ROUTE = route;